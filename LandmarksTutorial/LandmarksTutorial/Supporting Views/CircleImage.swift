//
//  CircleImage.swift
//  LandmarksTutorial
//
//  Created by Sean-MacbookPro on 2019-11-03.
//  Copyright © 2019 midfords. All rights reserved.
//

import SwiftUI

struct CircleImage: View {
    var body: some View {
        Image("turtlerock")
            .clipShape(Circle())
            .overlay(
                Circle().stroke(Color.white, lineWidth: 4))
            .shadow(radius: 10)
    }
}

struct CircleImage_Previews: PreviewProvider {
    static var previews: some View {
        CircleImage()
    }
}
