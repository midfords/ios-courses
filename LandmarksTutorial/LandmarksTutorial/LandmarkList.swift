//
//  LandmarkList.swift
//  LandmarksTutorial
//
//  Created by Sean-MacbookPro on 2019-11-04.
//  Copyright © 2019 midfords. All rights reserved.
//

import SwiftUI

struct LandmarkList: View {
    var body: some View {
        List(landmarkData) {landmark in
            LandmarkRow(landmark: landmark)
        }
    }
}

struct LandmarkList_Previews: PreviewProvider {
    static var previews: some View {
        LandmarkList()
    }
}
